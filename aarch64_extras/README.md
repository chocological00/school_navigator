## Additional Steps for AArch64
Run `sudo docker build -t orientdb .` to build your own OrientDB image for AArch64

Then follow steps in the README.md

Don't forget to specify `PORT=` and `ORIENTDB_ROOT_PASSWORD=` when running `docker-compose up --build -d`!
