# School Navigator
An interactive mobile map for in-building guidance.

Demo available at <https://nav.chocologic.al/>

## Setup/Deployment Information

### Database Info

This is the most complicated part of the setup.

The service expects to be able to query an OrientDB server for the `schools` database, which will contain a vertex for each school, made up of a `id` integer and a `name` string (210510, Howard High School for example). For each school, there should also be a database with the name that matches its `id`. 

The service *will not* set this up, and you must do it yourself. The rest of this guide assumes that you are using the `docker-compose.yaml` file to set up the database, but in other configurations it will still be mostly applicable. Therefore, you need to have docker and docker-compose installed.

### To create the initial database:

1. Use the following command to create a docker container so that you can populate the database. Provide some path to a folder that will be mounted and used for the database storage, as well as a root password.

   - In most configurations, the actual password doesn't matter since the ports will only be exposed on the local server.

   ```sh
   docker run -d --name orientdb -p 2424:2424 -p 2480:2480 \
       -v /home/user/full_path_to_databases_folder:/orientdb/databases \
       -e ORIENTDB_ROOT_PASSWORD=passw0rd! \
       orientdb
   ```

2. You can access the web interface at `localhost:2480` in order to create the databases as described above.

3. Create the file `data/school.json`. Hopefully you already have this file somewhere.

4. Run `npm i` to download packages, and `node utils/convert/load.js` to import the files into the database. This may take a bit and should print a lot of stuff to the screen.

5. At this point, you've created the database file you need, and can remove the database container. `docker container stop orientdb && docker container rm orientdb`

6. Copy the contents of the database folder (should be a couple of subfolders) from wherever you put it earlier into `data/databases`.

### Dockerize It

1. Create the file `data/DATA.json` with the following schema:

   ```json
   {	
           "DB": {
                   "addr": "orientdb",
                   "user": "root",
                   "pwd": "passw0rd!"
           }
   }
   ```

   - `pwd` is whatever your root password from above is, and `addr` is the address where the database is running.

2. At this point, everything is ready to be deployed. The rest of this section is purely informative.

We have a `.dockerignore` file to save space, since all of the folders below aren't necessary for the container to run, and some of these folders (node_modules) can be quite large.

```
.git
*Dockerfile*
*docker-compose*
node_modules
frontend/build
frontend/node_modules
data/databases
```

Here is the current `Dockerfile`:

```dockerfile
FROM node:14-alpine

RUN apk add --no-cache python make g++

WORKDIR /app

COPY ./ ./
RUN npm ci --quiet
RUN cd frontend && npm ci --quiet && npm run build && cd ..

CMD [ "node", "./bin/www" ]
```

What this does:

1. Use a node image, and install tools required for `node-gyp`
2. Use the `app` folder in the container for everything, and copy the data from the project (except in the ignore file) into the container.
3. Install node packages for both backend and frontend, and build the frontend TS.
4. When the container is run, it should run `node ./bin/www`.

We also have a `.env` file for various parameters:

```
PORT=8000
ORIENTDB_ROOT_PASSWORD=passw0rd!
```

- The port is what port you want the service to be visible from outside of the container, and the password is whatever you set up the DB with earlier.

Now here's the `docker-compose.yaml` file, which is the big stuff:

```yaml
version: "3"
services:
  schoolnav:
    image: schoolnav:latest
    container_name: schoolnav
    build:
      context: .
      dockerfile: Dockerfile
    ports:
      - "${PORT}:3000"
  orientdb:
    image: orientdb
    container_name: orientdb
    ports:
      - "2424:2424"
      - "2480:2480"
    volumes:
      - ./data/databases:/orientdb/databases
    environment:
      - "ORIENTDB_ROOT_PASSWORD=${ORIENTDB_ROOT_PASSWORD}"

```

What this does:

1. Creates two services, `schoolnav` and `orientdb`. By default, both these services share a network, probably called `school_navigator_default`. Note that above we set the DB address is `orientdb`, which is because each container is visible within the network under that name.
2. The `schoolnav` service will be built based on the `Dockerfile` in the directory, tagged `schoolnav:latest`, and container titled `schoolnav`.
3. By default the service runs itself on port `3000`, so we map this port inside the container to `$PORT` on the host, defined in the `.env` file above.
4. The `orientdb` doesn't need to be built, just pulled from the hub, and container is titled `orientdb`. We mount the database folder so it has all of the data, and set the environment variable for the password.
   1. I don't actually know if we need to open those two ports, but it can be helpful for debugging or management to have those ports (web interface)

### Deployment

1. All that is required is `docker-compose up --build -d`, which will build the image for `schoolnav` and create the containers, and start them.
   1. Note: These services will not restart on failure or system restart. It probably could be set in the compose file, but I didn't, so set it yourself.

The following is the `.gitlab-ci.yml` file that is used for our own deployment:

```yaml
cache:
  paths:
  - node_modules/

image: node:latest

stages:
  - test
  - deploy

code_quality:
<omitted>

deploy_prod:
  stage: deploy
  script:
    - 'curl --location --request GET "$DEPLOY_ADDR" --header "Authorization: Basic $DEPLOY_AUTH"'
  environment:
    name: production
    url: https://$URL
  only:
  - master
```

Information about this:

1. Gitlab sets up a container to do these steps, which we use the `node` image for, even though in this case it probably isn't necessary.
2. The `node_modules` folder is cached, which speeds things up in tests, though we don't have tests so it's not that important.
3. For each push to master, that `curl` script will be run, which sends a request to our [deployment server](https://gitlab.com/drp19/deploymentService) (It basically just does the docker-compose command on a server)
