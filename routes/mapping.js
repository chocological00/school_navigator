const express = require('express');
const router = express.Router();
const db = global.db;

/* GET path map */
router.get('/', async (req, res, next) =>
{
	res.render('mapping/get');
});

router.post('/', async(req, res) =>
{
	const { rows, columns } = req.body;
	res.render('mapping/post', { rows: rows, columns: columns });
});

router.post('/save', async(req, res) =>
{
	const { schoolname, schoolcode, array, poi } = req.body;
	
	await db.execute('INSERT INTO school (name, code, location, map) VALUES (?, ?, ?, ?)', [schoolname, schoolcode, poi, array]);

	res.redirect('/');
});

module.exports = router;
