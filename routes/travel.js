const express = require('express');
const router = express.Router();
const db = global.db;

/* GET path map */
router.get('/', async (req, res, next) =>
{
	let [data] = await db.query('SELECT name,code,location FROM school');
	console.log(JSON.stringify(data));
	res.render('travel', { schools: JSON.stringify(data) });
});

module.exports = router;
 