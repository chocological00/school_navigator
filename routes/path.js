const express = require('express');
const router = express.Router();
const createError = require('http-errors');
const db = global.db;
const PF = require('pathfinding');

/* GET path map */
router.get('/', async (req, res, next) =>
{
	let school = req.query.school, from = req.query.from, to = req.query.to;
	if (!school)
	{
		return next(createError(400));
	}

	let [[data]] = await db.execute('SELECT map,location FROM school WHERE code = ?;',[school]); 
	if (!data) 
	{
		console.log("bad sql");
		return next(createError(400));
	}

	if (!(from && to)) 
	{
		res.send([data.map,data.location]);
	}

	let location = JSON.parse(data.location), map = JSON.parse(data.map);
	from = from.replace(/[^A-z0-9 ]/gm,'');
	to = to.replace(/[^A-z0-9 ]/gm,'');

	if (!(location[from]&&location[to]))
	{
		console.log("no location");
		return next(createError(400));
	}


	let points = [location[from].x,location[from].y,location[to].x,location[to].y];
	let grid = new PF.Grid(map);
	let finder = new PF.AStarFinder();
	let path;
	try
	{
		path = finder.findPath(...points,grid);
	} 
	catch (err) 
	{
		console.log("no path")
		return next(createError(400));
	}

	let newPath = PF.Util.compressPath(path);
	res.send(newPath);
});

module.exports = router;
