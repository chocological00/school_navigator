const express = require('express');
const router = express.Router();
const createError = require('http-errors');
const orient = require('../utils/orient.js');
const graphJS = require('../utils/graph.js');
const cors = require('cors');

/* GET list of schools */
router.get('/', cors(), async (req, res, next) =>
{
	let dbs = await orient.getDBs();
	console.log(dbs);
	if (!dbs) return next(createError(500));
	res.send(dbs);
});

/* GET school data */
router.get('/:id', cors(), async (req,res,next) =>
{
	let data = await orient.getSchoolGraph(req.params.id);
	if (!data) return next(createError(500));
	let promiseNodes = [];
	let nodes = [];
	const push = (node) =>
	{
		nodes.push({"id":node.id,"name":node.data.name,"type":node.data.type});
	};
	data.forEachNode((node) => {
		promiseNodes.push(push(node));
	});
	await Promise.all(promiseNodes);
	res.send(nodes);
});

/* GET path from a to b */
router.get('/:id/:from\::to', cors(), async (req,res,next) => // eslint-disable-line no-useless-escape
{
	let data = await orient.getSchoolGraph(req.params.id);
	if (!data) return next(createError(500));
	
	let found = await graphJS.find(data,req.params.from, req.params.to);
	if (!found) return next(createError(400));
	res.send(found);
});

module.exports = router;
