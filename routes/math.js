const math = require('mathjs');

function integrate (f, start, end, step=.01) {
	let total = 0;
	step = step || 0.01;
	for (let x = start; x < end; x += step) {
		total += f(x + step / 2) * step;
	}
	return total;
}

integrate.transform = function (args, math, scope) {
	// determine the variable name
	if (!args[1].isSymbolNode) {
		throw new Error('Second argument must be a symbol');
	}
	const variable = args[1].name;

	// evaluate start, end, and step
	const start = args[2].compile().eval(scope);
	const end = args[3].compile().eval(scope);
	const step = args[4] && args[4].compile().eval(scope); // step is optional

	// create a new scope, linked to the provided scope. We use this new scope
	// to apply the variable.
	const fnScope = Object.create(scope);

	// construct a function which evaluates the first parameter f after applying
	// a value for parameter x.
	const fnCode = args[0].compile();
	const f = function (x) {
		fnScope[variable] = x;
		return fnCode.eval(fnScope);
	};

	// execute the integration
	return integrate(f, start, end, step);
};

integrate.transform.rawArgs = true;

math.import({
	integrate: integrate
});

let a = math.eval('integrate(cos(x), x, 0, 4)'); 
console.log(a+"\n" + (a-(-0.756802495308)));
