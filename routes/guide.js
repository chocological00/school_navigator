const express = require('express');
const router = express.Router();
const db = global.db;

/* GET path map */
router.get('/', async (req, res, next) =>
{
	const path = JSON.parse(req.query.path);
	const from = req.query.from;
	const to = req.query.to;
	res.render('guide', { path: path, from: from, to: to });
});

module.exports = router;
 