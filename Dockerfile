FROM node:14-alpine

RUN apk add --no-cache python make g++

WORKDIR /app

COPY ./ ./
RUN npm ci --quiet
RUN cd frontend && npm ci --quiet && npm run build && cd ..

CMD [ "node", "./bin/www" ]

