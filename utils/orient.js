const OrientDBClient = require('orientjs').OrientDBClient;
const graphJS = require('./graph.js');
//const toJSON = require('ngraph.tojson');
let client;
const data = JSON.parse(require('fs').readFileSync('data/DATA.json')).DB;
const init = async (db = null) =>
{
	try 
	{
		if (!client) 
		{
			client = await OrientDBClient.connect({ host: data.addr });
			console.log('Connected');
		}
		if (db)
		{
			let session = await client.session({
				name: db,
				username: 'admin',
				password: 'admin'
			});
			return session;
		}
	} 
	catch (e) 
	{
		console.log(e);
		return false;
	}		
};

const processNode = async (node,graph) =>
{
	let data = 
	{
		'id' : node.id, 
		'name' : node.name,
		'rid' : node['@rid'],
		'type' : node['@class']
	};
	switch(node['@class'])
	{
	case 'hallway' : data.length = node.length, data.traffic = node.traffic; break;
	case 'stairs' : data.floor = node.floor; break;
	}
	graph.addNode(node.id,data);
	return 1;
};
const processLink = async(link,session,graph) =>
{
	let from = await session.select().from(`#${link.out.cluster}:${link.out.position}`).one();
	let to = await session.select().from(`#${link.in.cluster}:${link.in.position}`).one();
	let data = 
	{
		//'fromRID' : from['@rid'],
		//'toRID' : to['@rid'],
		'type' : link['@class']
	};
	switch(link['@class'])
	{
	case 'rH' : case 'sH' : data.pos = link.pos, data.side = link.side; break;
	case 'hI' : case 'sI' : data.angle = link.angle; break;
	}
	graph.addLink(from.id,to.id,data);
	return 1;
};


const getDBs = async() =>
{
	try 
	{
		let session = await init('schools');
		let databases = await client.listDatabases({
			username: data.user,
			password: data.pwd
		});
		let names = await session.select('name','id').from('v').all();
		let newNames = [];
		for (let i of names) 
		{
			if (databases[i.id]) newNames.push(i);
		}
		//console.log(newNames);
		await session.close();
		return newNames;
	} 
	catch (e) 
	{
		console.log(e);
		return false;
	}
};
module.exports.getDBs = getDBs;


const getSchoolGraph = async (school) => 
{
	try 
	{
		let session = await init(school);
		let graph = graphJS.graph;
		let promiseNodes = [], promiseLinks = [];
		let nodes = await session.select().from('v').all();
		//console.log(data);
		nodes.forEach((item) => 
		{
			promiseNodes.push(processNode(item,graph));
		});
		await Promise.all(promiseNodes);

		let links = await session.select().from('e').all();
		links.forEach( (item) => 
		{
			promiseLinks.push(processLink(item,session,graph));
		});
		await Promise.all(promiseLinks);

		console.log('after');
		//close session
		await session.close();
		return graph;
	} 
	catch (e) 
	{
		console.log(e);
		return false;
	}

};
module.exports.getSchoolGraph = getSchoolGraph;
