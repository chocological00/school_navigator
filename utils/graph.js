const path = require('ngraph.path');
const toJSON = require('ngraph.tojson');
const fromJSON = require('ngraph.fromjson');
const ngraph = require('ngraph.graph');
const graph = ngraph();
module.exports.graph = graph;

const dist = (data,from,to,link,start,end) =>
{
	if (link.fromId!==from.id) 
	{
		let temp = to;
		to=from,from=temp;
	}
	switch (from.data.type)
	{
	case 'room': return 100;//should not cut through rooms
	case 'stairs' : {
		if (to.data.type==='stairs') return 100;
		else if (link.data.hall==null) return 1; // eslint-disable-line eqeqeq  from intersection to stair naturally
		else return data.getNode(link.data.hall).data.length*(Math.abs(link.data.base-link.data.pos)); //in hallway
	}
	case 'hallway' : {
		let len = from.data.length;
		let base = (link.data.angle+45)%360<180?1:0; //gets direction going towards (0L,1R)
		let roomPos,finalLen;
		data.forEachLinkedNode(from.id,(node,link) => //get position of originating/ending room, or use full length
		{
			if (node.id===start) 
				roomPos = link.data.pos;
			if (node.id===end) 
				roomPos = link.data.pos;	
		});
		if (!roomPos) 
			finalLen = len;
		else 
			finalLen = len*(Math.abs(base-roomPos));
		return finalLen;
	}
	default: return 1;
	}
};

const find = (data,start,finish) =>
{
	let nData = fromJSON(toJSON(data)); //make a copy of `data` for changes
	data.forEachLink((link) => 
	{
		if (!link) return;
		if (link.data.type==='sH')
		{
			data.forEachLinkedNode(link.toId,(linkedNode, hallLink) =>
			{
				if (linkedNode.data.type==='intersection') // find all intersections connected to the hallway connected to the stairs
				{
					if (!nData.getLink(link.fromId)) //create a temp connection to the intersection from stairs
						nData.addLink(link.fromId,linkedNode.id,{'pos':link.data.pos,'side':link.data.side,'hall':link.toId,'base':(hallLink.data.angle+45)%360<180?1:0});
				}
			});
			//nData.removeLink(nData.getLink(link.fromId,link.toId)); //temp remove old s-H intersection EDIT: DONT NEED TO DO THIS
		}
	});
	data = nData;
	let pathFinder = path.aStar(data,
		{
			distance (fromNode,toNode,link)
			{
				return Math.abs(dist(data,fromNode,toNode,link,start,finish));
			}
		});
	let found;
	try 
	{
		found = pathFinder.find(start,finish);
	} 
	catch (e) 
	{
		return;
	}
	if (found.length===0)
		return false;
	found = found.reverse(); //returns array backwards, so flip

	let ret = [];
	let on; //saved with side of a hall a room/stair is on for next iteration
	for (let i = 0; i < found.length-1; i++) //loop through path elements
	{
		let fromNode = found[i],toNode = found[i+1];
		let link = data.getLink(fromNode.id,toNode.id)?data.getLink(fromNode.id,toNode.id):data.getLink(toNode.id,fromNode.id);
		let length = dist(data,fromNode,toNode,link,start,finish);
		found[i].length = length;

		let itemData = {'type':fromNode.data.type,'id':fromNode.id};
		let secondData = null;
		switch(fromNode.data.type)
		{
		case 'room' : case 'stairs' : 
		{
			if (fromNode.data.type==='stairs')
			{
				itemData.type='stair_enter'; //entering stairs
				itemData.on = on; //will be on a side, straight, or null for from intersection
				ret.push(itemData);

				itemData = {'type':'stair_travel','id':fromNode.id}; //in stairs
				let fromFloor = fromNode.data.floor;
				while (found[i+1].data.type==='stairs') //loop through inner stair nodes/ignore them
				{
					if (!found[i+2]) break;
					i++;
				}
				let toFloor = found[i].data.floor;
				if (toFloor-fromFloor===0) //if using stairs as shortcut
				{
					itemData = null;
					ret.pop();
					break;
				}
				itemData.floors= toFloor-fromFloor;
				ret.push(itemData);

				fromNode = found[i],toNode = found[i+1]; //reset from and to after looping
				itemData = {'type':'stair_exit','id':fromNode.id}; //leaving stairs
				link = data.getLink(fromNode.id,toNode.id)?data.getLink(fromNode.id,toNode.id):data.getLink(toNode.id,fromNode.id);
				
			}
			if (toNode.data.type!=='intersection') //if going to an intersection, no exit direction
			{
				let side = link.data.side;
				if (side===null) itemData.exit = 's';
				else if (!found[i+2]) itemData.exit = 's'; //if bad request, the toNode could be the hallway right outside
				else if (found[i+2].data.type==='room'||found[i+2].data.type==='stairs') 
				{
					//if going to a room in the same hall, get the relative distance between and determine direction of travel
					let dir = (data.getLink(found[i+2].id,toNode.id).data.pos-link.data.pos)>0;
					itemData.exit = side===dir?'l':'r';
				}
				else 
				{
					//if leaving hallway, find the direction of travel
					let dir = data.getLink(toNode.id,found[i+2].id).data.angle+45<180;
					itemData.exit = side===dir?'l':'r';
				}
			}
			break;
		}
		case 'hallway' : 
		{
			if (toNode.data.type==='room'||toNode.data.type==='stairs')
			{
				let side = link.data.side;
				if (!found[i-1]) //bad request, starting in hallway
					on = null, itemData.len = null;
				else if (found[i-1].data.type==='room'||found[i-1].data.type==='stairs') 
				{
					//determine which side room is on based on direction of travel from room, set length
					let dir = link.data.pos-data.getLink(found[i-1].id,fromNode.id).data.pos;
					if (side===null) on = 's';
					else on = side===dir>0?'l':'r';
					itemData.len = Math.abs(dir*fromNode.data.length);
				}
				else
				{
					//determine which side room is on based on direction of travel from other interesection
					let ang = data.getLink(fromNode.id,found[i-1].id).data.angle+45;
					if (side===null) on = 's';
					else on = side===ang<180?'r':'l';
					itemData.len = found[i-1].length;
				}
				
			} 
			else 
			{
				itemData.len = length; 
			}
			if (ret[ret.length-1].id===fromNode.id) //if stairs divided hallway, combine
				itemData.len += ret.pop().len;
			break;
		}
		case 'intersection' : 
		{
			let angle1,angle2;
			let newLink = data.getLink(found[i-1].id,fromNode.id); 
			// eslint-disable-next-line eqeqeq
			if (found[i-1].data.type==='stairs'&&newLink.data.hall!=null) //coming from hallway stairs, going to intersection
			{
				angle1 = data.getLink(newLink.data.hall,fromNode.id).data.angle; //use hallway for angle
				let len = data.getNode(newLink.data.hall).data.length*(Math.abs(newLink.data.base-newLink.data.pos));
				let dir = angle1+45<180;
				let exit;
				if (newLink.data.side===null) exit = 's'; //same as normal room side calculation
				else exit = newLink.data.side===dir?'l':'r';
				ret[ret.length-1].exit=exit; //modify stairs to have correct exit data
				if (ret[ret.length-1].id===newLink.data.hall) //if stairs divided hallway, combine
					len += ret.pop().len;
				ret.push({'type':'hallway','id':newLink.data.hall,'len':len}); //add the previous hallway
			} 
			else
				angle1 = data.getLink(found[i-1].id,fromNode.id).data.angle;
			// eslint-disable-next-line eqeqeq
			if (toNode.data.type==='stairs'&&link.data.hall!=null) //going to hallway stairs from intersection
			{
				angle2 = data.getLink(link.data.hall,fromNode.id).data.angle; //use original hallway for angle
				let len = data.getNode(link.data.hall).data.length*(Math.abs(link.data.base-link.data.pos)); //normal length calculation
				secondData={'type':'hallway','id':link.data.hall,'len':len}; //after adding intersect, add hallway before stairs
				let side = link.data.side; //normal side calculation
				if (side===null) on = 's';
				else on = side===angle2+45<180?'r':'l';
			}
			else
				angle2 = link.data.angle;
			let diff = angle2-((angle1+180)%360); //determine relative angle
			itemData.angle = diff;
			break;
		}
		}
		itemData?ret.push(itemData):null;
		secondData?ret.push(secondData):null;
	}
	//add last item (added seperately to prevent out of bound error with 'toNode')
	ret.push({'type':found[found.length-1].data.type,'id':found[found.length-1].id,'on':on});
	return ret;
};
module.exports.find = find;

const json = (g) =>
{
	let json = JSON.parse(toJSON(g));
	return json;
};
module.exports.json = json;