var createGraph = require('ngraph.graph');
var g = createGraph();
var tojson = require('ngraph.tojson');
var fs = require('fs'); 
var path = require('ngraph.path');

const main = async () => {
	let arrIn = fs.readFileSync('fsIn.json');
	let arr = JSON.parse(arrIn);
	for(var i = 0; i < arr.length; i++) {
		var arrI = arr[i];
		for(var j = 0; j < arrI.length; j++) {
			if (arrI[j] == 1) { //TODO: THIS IS TEMPORARY
				g.addNode(`x${j}y${i}`);
				//console.log(`x${j}y${i}`)
			}
		}
	}
	g.forEachNode((node) =>
	{
		let matches = /x(\d+)y(\d+)/gm.exec(node.id);
		let x = matches[1],y = matches[2];
		let xP = `x${parseInt(x)+1}y${y}`,xN = `x${x}y${parseInt(y)+1}`;
		let yP = `x${parseInt(x)-1}y${y}`,yN = `x${x}y${parseInt(y)-1}`;
		if (g.getNode(xP)) if (!g.getLink(xP,node.id)) g.addLink(node.id, xP);
		if (g.getNode(xN)) if (!g.getLink(xN,node.id)) g.addLink(node.id, xN);
		if (g.getNode(yP)) if (!g.getLink(yP,node.id)) g.addLink(node.id, yP);
		if (g.getNode(yN)) if (!g.getLink(yN,node.id)) g.addLink(node.id, yN);
	});
	let stringGraph = tojson(g);

	fs.writeFileSync('fsOut.json', JSON.stringify(stringGraph));
	let pathfinder = path.aGreedy(g);
	let foundPath = pathfinder.find("x3y6", "x4y10");
	console.log(JSON.stringify(foundPath));
};

main();
//console.log(graphLib.json.write(g));