var createGraph = require('ngraph.graph');
var g = createGraph();
var fs = require('fs'); 
var request = require('request');

const DB_URL = 'http://localhost:2480';
const schoolID = 210510;
const main = async () => 
{
	let arrIn = await JSON.parse(fs.readFileSync('data/school.json'));
	let nodes = arrIn.nodes, links = arrIn.links;
	
	for (let key in nodes) 
	{
		if (nodes.hasOwnProperty(key)) 
		{
			let nS = nodes[key];
			for (let i of nS)
				g.addNode(i.id,i);
		}
	}

	for (let key in links) 
	{
		if (links.hasOwnProperty(key)) 
		{
			let lS = links[key];
			for (let l of lS)
				g.addLink(l.from,l.to,l);
		}
	}

	let options = {
		url: DB_URL + '/connect/'+schoolID,
		method: 'GET',
		headers: {
			'Authorization': 'Basic YWRtaW46YWRtaW4='
		}
	};
	function connect() 
	{
		return new Promise(function(resolve,reject) 
		{
			request(options, function (error, response, body) 
			{
				if (response.statusCode == 204) 
				{
					resolve(true);
				}
				else
					reject(error);
			});
		});
	}

	let conResult = await connect();
	if (conResult !== true) 
	{
		console.error('Could not connect to server.');
		return;
	}

	let schema = await fs.readFileSync('utils/convert/default_schema.json');
	options = {
		url: DB_URL + '/import/'+schoolID,
		method: 'POST',
		body : schema,
		headers: {
			'Authorization': 'Basic YWRtaW46YWRtaW4='
		}
	};
	function imp() 
	{
		return new Promise(function(resolve) 
		{
			request(options, function (error, response, body) 
			{
				resolve(body);
			});
		});
	}
	let impResult = await imp();
	//console.log(impResult);

	let catcher = []; // in the rye

	g.forEachNode(lsNode);

	function lsNode (node) 
	{
		catcher.push(new Promise((resolve) =>
		{
			//console.log(node);
			let data;
			try {
				data = {'id' : node.data.id , 'name' : node.data.name};
			} catch (e) 
			{
				console.log(node);
			}
			let type = '';
			switch (node.data.type) 
			{
			case 'i' : 
				type = 'intersection'; 
				break;
			case 'h' : 
				type = 'hallway'; 
				data['length'] = node.data.length, data['traffic'] = node.data.traffic; 
				break;
			case 'r' : 
				type = 'room';
				break;
			case 's' : 
				type = 'stairs';
				data['floor'] = node.data.floor;
				break;
			}
			data['@class'] = type;
			options = {
				url: DB_URL + '/document/'+schoolID,
				method: 'POST',
				body : data,
				json : true,
				headers: {
					'Authorization': 'Basic YWRtaW46YWRtaW4='
				}
			};
	
			request(options, (error, response, body) =>
			{
				let rid = body['@rid'];
				console.log(rid);
				node.data.rid = rid;
				g.addNode(node.data.id,node.data);
				resolve();
			});
		}));
	}
	await Promise.all(catcher);

	catcher = [];

	g.forEachLink(lsLink);

	function lsLink (link) 
	{
		catcher.push(new Promise((resolve) =>
		{
			let outRID = g.getNode(link.fromId).data.rid.replace('#',''), inRID = g.getNode(link.toId).data.rid.replace('#','');
			let data = {'in': inRID, 'out' : outRID};
			data['@class'] = link.data.type;
			switch(link.data.type)
			{
			case 'rH' : case 'sH' : data['pos'] = link.data.pos, data['side'] = link.data.side; break;
			case 'hI' : case 'sI' : data['angle'] = link.data.angle; break;
			}
			options = {
				url: DB_URL + '/document/'+schoolID,
				method: 'POST',
				body : data,
				json : true,
				headers: {
					'Authorization': 'Basic YWRtaW46YWRtaW4='
				}
			};

			request(options, (error, response, body) =>
			{
				console.log(body);
				resolve();
			});
			
		}));
	}

	await Promise.all(catcher);
};

main();