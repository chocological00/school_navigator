// dependencies
const fs = require('fs');
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const flash = require('connect-flash');
const mysql = require('mysql2/promise');
const session = require('express-session');
const MySQLStore = require('express-mysql-session')(session);

// app will be exported so it can be compiled
const app = express();

// main function
const main = async () =>
{
	app.locals.pretty = true;

	// view engine setup
	app.set('views', path.join(__dirname, 'views'));
	app.set('view engine', 'pug');
	
	// Database
	process.stdout.write('Connecting to the Database... ');
//	const DATA = JSON.parse(fs.readFileSync('data/DATA.json'));
//	global.db = await mysql.createConnection(
//		{
//			host: 'localhost',
//			user: DATA.SQL.username,
//			password: DATA.SQL.password,
//			database: DATA.SQL.database
//		}
//	);
	console.log('Done!'); // eslint-disable-line no-console

	// Session
	process.stdout.write('Initializing Session Storage... ');
//	const sessionStore = new MySQLStore(
//		{
//			host: 'localhost',
//			port: 3306,
//			user: DATA.SQL.username,
//			password: DATA.SQL.password,
//			database: 'test_db'
//		}
//	);
//	app.use(session(
//		{
//			key: 'bowsette',
//			secret: 'a_terrible_password',
//			store: sessionStore,
//			resave: false,
//			saveUninitialized: false
//		}
//	));
	console.log('Done!'); // eslint-disable-line no-console

	// middlewares
	process.stdout.write('Initializing Middlewares... ');
	app.use(logger('dev'));
	app.use(express.json());
	app.use(express.urlencoded({ extended: true }));
	app.use(cookieParser('nyan_cat'));
	app.use(flash());
	console.log('Done!'); // eslint-disable-line no-console

	// routing
	// dependencies
	app.use(express.static(path.join(__dirname, 'frontend/build'))); // static route
	app.use('/users', require('./routes/users'));
	app.use('/path', require('./routes/path'));
	app.use('/mapping', require('./routes/mapping'));
	app.use('/school', require('./routes/school'));

	// reply with react for all requests
	app.use((req, res, next) => 
	{
		res.sendFile(path.join(__dirname, 'frontend/build/index.html'));
	});

	// error handler
	app.use((err, req, res, next) =>
	{
		// set locals, only providing error in development
		res.locals.message = err.message;
		res.locals.error = req.app.get('env') === 'development' ? err : {};

		// render the error page
		res.status(err.status || 500);
		res.render('error');
	});
};

main();

module.exports = app;
