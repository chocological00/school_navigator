import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { Home, Guidance, About, Nonexistant } from '../pages';
import Particles from 'react-particles-js';

class App extends Component
{
	render()
	{
		return(
			<React.Fragment>
				<Switch>
					<Route exact path = '/' component = { Home } />
					<Route path = '/guidance/:school/:from\::to' component = { Guidance } />
					<Route path = '/about' component = { About } />
					<Route component = { Nonexistant } />
				</Switch>
				<div className='shaded-box fullscreen-container'></div>
				<Particles
					className='background fullscreen-container'
					params={{"particles": {"number": {"value": 100,"density": {"enable": true,"value_area": 1500}},"line_linked": {"enable": true,"opacity": 0.02},"move": {"direction": "right","speed": 0.05},"size": {"value": 1},"opacity": {"anim": {"enable": true,"speed": 1,"opacity_min": 0.05}}},"interactivity": {"detect_on": "window","events": {"onclick": {"enable": true,"mode": "push"}},"modes": {"push": {"particles_nb": 1}}},"retina_detect": true}} 
				/>
			</React.Fragment>
		)
	}
};

export default App;