import React from 'react';
import  { Link } from 'react-router-dom';
import axios from 'axios';

class Home extends React.Component
{
	state = 
	{
		school_list: [{id: 'error', name: 'Loading... please wait'}],
		selected_school_id: '',
		selected_school_obj: [{id:'',name:'',type:''}],
		selected_room_from: '',
		selected_room_to: ''
	};

	handleSchoolSelectChange = (e: any) =>
	{
		this.setState(
			{
				selected_school_id: e.target.value
			});
	};

	getRooms = async (e: any) =>
	{
		const { data } = await axios.get(`/school/${this.state.selected_school_id}`);
		data.sort((a: any, b: any) => (a.name).localeCompare(b.name));
		this.setState(
			{
				selected_school_obj: data
			});
	};

	handleRoomSelectFromChange = (e: any) =>
	{
		this.setState(
			{
				selected_room_from: e.target.value
			})
	};

	handleRoomSelectToChange = (e: any) =>
	{
		this.setState(
			{
				selected_room_to: e.target.value
			})
	};

	render = () =>
	{
		const school_select_options = this.state.school_list.map((obj, index) =>
		{
			return(<option key={index} value={obj.id}>{obj.name}</option>);
		});

		const school_select_button = (() => 
		{
			if(this.state.selected_school_id === '' || this.state.selected_school_id === 'error')
				return(<button type='button' className='btn btn-info' disabled>Go!</button>)
			else
				return(<button type='button' className='btn btn-info' data-toggle='modal' data-target='#room_select_modal' onClick={this.getRooms}>Go!</button>)
		})();

		const room_select_options = this.state.selected_school_obj.filter((obj) =>
		{
			if(obj.type !== 'room') return false;
			return true;
		}).map((obj, index) =>
		{
			if(obj.id === this.state.selected_room_from || obj.id === this.state.selected_room_to)
				return(<option key={index} value={obj.id} disabled>{obj.name}</option>);
			return(<option key={index} value={obj.id}>{obj.name}</option>);
		});

		const start_guidance_button = (() =>
		{
			if(this.state.selected_room_from !== '' && this.state.selected_room_from !== 'error' && this.state.selected_room_to !== '' && this.state.selected_room_to !== 'error')
				return(<Link to={`/guidance/${this.state.selected_school_id}/${this.state.selected_room_from}:${this.state.selected_room_to}`}><button type='button' className='btn btn-info'>Start Guidance</button></Link>)
			else
				return(<button type='button' className='btn btn-info' disabled>Start Guidance</button>)
		})();

		return(
			<React.Fragment>
				<div className='center-container fullscreen-container'>
					<div>
						<div className='card text-center card-nav-tabs' style={{opacity: 0.8, width: '20rem'}}>
							<h3 className='card-header card-header-info'>Welcome!</h3>
							<div className='card-body'>
								{React.createElement('label', {htmlFor: 'school-select'}, 'Select School')}
								<select className='form-control selectpicker text-center' id='school-select' onChange={this.handleSchoolSelectChange} defaultValue='error'>
									<option value='error' disabled>Pick an option...</option>
									{school_select_options}
								</select>
								<div style={{lineHeight: '80%'}}>
									<br />
									<small>
										Proudly brought to you by<br />
										<Link to='/about' style={{color: '#20a0b0'}}>{'>cool team name goes here<'}</Link>
									</small>
									<br /><br />
								</div>
								{school_select_button}
							</div>
						</div>
					</div>
				</div>
				<div className='modal fade' id='room_select_modal' tabIndex={-1} role='dialog' area-labeledby='room_select_modal_label' area-hidden='true'>
					<div className='modal-dialog' role='document'>
						<div className='modal-content'>
							<div className='modal-header'>
								<h5 className='modal-title' id='room_select_modal_label'>Where would you like to go today?</h5>
								<button type='button' className='close' data-dismiss='modal' area-label='Close'>
								</button>
							</div>
							<div className='modal-body'>
								<label htmlFor='room-select-from'>From:</label>
								<select className='form-control selectpicker' id='room-select-from' onChange={this.handleRoomSelectFromChange} defaultValue='error'>
									<option value='error' disabled>Select your starting point</option>
									{room_select_options}
								</select>
								<label htmlFor='room-select-to'>To:</label>
								<select className='form-control selectpicker' id='room-select-to' onChange={this.handleRoomSelectToChange} defaultValue='error'>
									<option value='error' disabled>Select your destination</option>
									{room_select_options}
								</select>
								<br />
							</div>
							<div className='modal-footer'>
								<button type='button' className='btn btn-secondary' data-dismiss='modal'>Cancel</button>
								{start_guidance_button}
							</div>
						</div>
					</div>
				</div>
			</React.Fragment>		
		)
	};

	componentDidMount = async () =>
	{
		const { data } = await axios.get(`/school`);
		data.sort((a: any, b: any) => (a.name).localeCompare(b.name));
		this.setState(
			{
				school_list: data
			});
	};

	componentWillUnmount = () =>
	{
		// react doesn't play nice with non-react bootstrap js, so just remove it manually
		const modal_backdrop = document.getElementsByClassName('modal-backdrop').item(0);
		if(modal_backdrop) modal_backdrop.remove();
	}
};

export default Home;