import React from 'react';
import Swiper from 'swiper';
import MobileDetect from 'mobile-detect';
import axios from 'axios';

class Guidance extends React.Component
{
	state = 
	{
		school: '',
		from: '',
		to: '',
		schoolobj: {},
		guide: [{guide: '',currentlyAt: '',direction: '',iconClass:''}]
	}

	render = () =>
	{
		const guideSets = this.state.guide.map((obj, index) =>
		{
			let BigText: any = 'h1'
			let SmallText: any = 'h3';

			if(window.innerHeight > window.innerWidth || (new MobileDetect(window.navigator.userAgent)).phone()) // small screen
			{
				BigText = 'h3';
				SmallText = 'p';
			}

			return(
				<div key={index} className='swiper-slide fullscreen-container'>
						<div className='swiper-top'>
							<div className='card full-size-card'>
								<div className='card-body card-body-center'>
									<div><BigText><i className='fas fa-compass'></i>&nbsp;{obj.guide}</BigText></div>
									<div>
										<div><SmallText><i className='fas fa-map-marker-alt'></i>&nbsp;Currently at: {obj.currentlyAt}</SmallText></div>
										<div><SmallText><i className='fas fa-route'></i>&nbsp;Guiding to: {this.state.to}</SmallText></div>
									</div>
								</div>
							</div>
						</div>
						<div className='swiper-bottom'>
							<i className={obj.iconClass} style={{transform: `rotate(${obj.direction})`}}></i>
						</div>
					</div>
			);
		});

		return(
			<div className='fullscreen-container'>
				<div className='swiper-container'>
					<div className='swiper-wrapper'>
						{guideSets}
					</div>
					<div className='swiper-pagination'></div>
				</div>
			</div>
		)
	}
	componentDidMount = async () =>
	{
		// @ts-ignore
		const { school, from, to } = this.props.match.params;
		const schoolobj = (await axios.get(`/school/${school}`)).data;
		const getNameFromId = (id: string): string =>
		{
			// @ts-ignore
			for(let obj of schoolobj) if(id === obj.id) return obj.name;
			return '';
		};
		this.setState(
			{
				school: school,
				from: from,
				to: getNameFromId(to)
			});
		const { data } = await axios.get(`/school/${school}/${from}:${to}`);
		const guide = data.map((obj: any, index: number) =>
		{
			const toPush = 
			{
				guide: 'Error',
				currentlyAt: getNameFromId(obj.id),
				direction: '0deg',
				iconClass: 'fas fa-arrow-up fa-10x'
			};
			switch(obj.type)
			{
				case 'room':
					if(obj.exit)
					{
						switch(obj.exit)
						{
							case 'l': 
								toPush.guide = `Exit left to ${getNameFromId(data[index+1].id)}`; 
								toPush.direction = '-90deg';
								break;
							case 'r': 
								toPush.guide = `Exit right to ${getNameFromId(data[index+1].id)}`;
								toPush.direction = '90deg'; 
								break;
							default: 
								toPush.guide = `Exit straight to ${getNameFromId(data[index+1].id)}`;
								break;
						}
					}
					else
					{
						switch(obj.on)
						{
							case 'l':
								toPush.guide = `${toPush.currentlyAt} is on your left`;
								toPush.direction = '-90deg';
								break;
							case 'r':
								toPush.guide = `${toPush.currentlyAt} is on your right`;
								toPush.direction = '90deg';
								break;
							default:
								toPush.guide = `You have arrived at ${toPush.currentlyAt}`;
								break;
						}
					}
					break;
				case 'hallway':
					toPush.guide = `Walk through the ${toPush.currentlyAt} for ${obj.len.toPrecision(2)} meters`;
					break;
				case 'intersection':
					if(obj.angle === 0)
					{
						toPush.guide = `Continue walking ${getNameFromId(data[index-1].id)===getNameFromId(data[index+1].id)?'through':'to'} ${getNameFromId(data[index+1].id)}`;
						toPush.direction = `${obj.angle}deg`;
					}
					else
					{
						toPush.guide = `Turn to ${getNameFromId(data[index+1].id)}`;
						toPush.direction = `${obj.angle}deg`;
					}
					break;
				case 'stair_enter': 
					switch(obj.on)
					{
						case 'l':
							toPush.guide = `Enter the ${toPush.currentlyAt} on your left`;
							toPush.direction = '-90deg';
							break;
						case 'r':
							toPush.guide = `Enter the ${toPush.currentlyAt} on your right`;
							toPush.direction = '90deg';
							break;
						default:
							toPush.guide = `Enter the ${toPush.currentlyAt}`;
							break;
					}
					break;
				case 'stair_travel': 
					if (obj.floors>0) 
					{
						toPush.guide = `Go up ${obj.floors} floor${obj.floors>1?'s':''}`;
						toPush.direction = '180deg'; // flip the icon so down is up and up is down
						toPush.iconClass = 'fas fa-sort-amount-down fa-10x';
					}
					else
					{
						toPush.guide = `Go down ${-1*obj.floors} floor${obj.floors>1?'s':''}`;
						toPush.direction = '180deg'; // and true is false and illuminatti is real
						toPush.iconClass = 'fas fa-sort-amount-up fa-10x';
					}
					break;	
				case 'stair_exit': 
					if (obj.exit)
					{
						switch(obj.exit)
						{
							case 'l': 
								toPush.guide = `Exit left to ${getNameFromId(data[index+1].id)}`;
								toPush.direction = '-90deg';
								break;
							case 'r': 
								toPush.guide = `Exit right to ${getNameFromId(data[index+1].id)}`;
								toPush.direction = '90deg'; 
								break;
							default: 
								toPush.guide = `Exit forward to ${getNameFromId(data[index+1].id)}`;
								break;
						}
					}
					else
					{
						toPush.guide = `Exit the ${toPush.currentlyAt}`;
					}
					break;
			}
			return toPush;
		});
		guide.unshift(
			{
				guide: 'Swipe left to begin guidance',
				currentlyAt: getNameFromId(from),
				direction: '0deg',
				iconClass: 'fas fa-map-marked-alt fa-10x'
			});
		this.setState(
			{
				guide: guide
			});
		new Swiper('.swiper-container',
			{
				direction: 'horizontal',
				loop: false,
				pagination:
				{
					el: '.swiper-pagination',
					type: 'progressbar'
				},
				mousewheel: true
			});
	}
}

export default Guidance;