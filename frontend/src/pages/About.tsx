import React from 'react';

class About extends React.Component
{
	render()
	{
		return(
			<div className='center-container center-text'>
				<div style={{color: 'white'}}>
					Frontend by David Kim
					<br />
					API and pathfinding algorithm by Drew Pleat
				</div>
			</div>
		)
	}
}

export default About;